package com.example.test_list;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "testDB";
    private static final int DB_version = 1;
    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_version);
    }
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table if not exists Setting (" + "MODE INTEGER," + "PASSWORD TEXT);");
        insert_setting(db, 0, "000000");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static void insert_setting(SQLiteDatabase db, int mode, String password){
        ContentValues testValues = new ContentValues();
        testValues.put("MODE", mode);
        testValues.put("PASSWORD", password);
        db.insert("Setting", null, testValues);
    }
}