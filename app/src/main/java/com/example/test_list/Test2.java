package com.example.test_list;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.HideReturnsTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;

import static com.example.test_list.R.layout.activity_test2;

public class Test2 extends AppCompatActivity {
    SQLiteDatabase sqLiteDatabasedb;
    DBHelper dbHelper;
    Cursor cursor;
    EditText editText;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(activity_test2);
        Spinner spinner=(Spinner)findViewById(R.id.spinner);
        editText = (EditText)findViewById(R.id.editText4);
        editText.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        Button button = findViewById(R.id.button);

        dbHelper = new DBHelper(this);
        sqLiteDatabasedb = dbHelper.getReadableDatabase();

        cursor = sqLiteDatabasedb.rawQuery("SELECT name FROM sqlite_master WHERE type='table'AND name='Setting'", null);

        cursor = sqLiteDatabasedb.rawQuery("SELECT name FROM Setting", null);
        Log.d("TAG!!!!!!!!!!!!!","테이블 확인");
        if (cursor.moveToFirst()) {
            //   while(curSr.moveToNext()) 테이블의 튜플 갯수만큼 루프를 돈다
            Log.d("TABLE NAME:",cursor.getString(0));
        }else{
            sqLiteDatabasedb.execSQL("CREATE TABLE Setting (" + "MODE INTEGER, PASSWORD TEXT);");
            dbHelper.insert_setting(sqLiteDatabasedb, 0, "000000");
        }
        Log.d("TAG!!!!!!!!!!!!!","테이블 확인 완료");

        cursor = sqLiteDatabasedb.rawQuery("SELECT * FROM Setting", null);
        if(cursor.moveToFirst()){
            int chack = cursor.getInt(0);
            if(chack == 0){
                spinner.setSelection(0);
            }else if(chack == 1){
                spinner.setSelection(1);
            }else if(chack == 2){
                spinner.setSelection(2);
            }
            editText.setText(cursor.getString(1));
        }

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(parent.getItemAtPosition(position).equals("알람모드")){
                    if(cursor.moveToFirst()){
                        sqLiteDatabasedb.execSQL("UPDATE Setting SET MODE = 0");
                    }
                }else if(parent.getItemAtPosition(position).equals("모드유지")){
                    if(cursor.moveToFirst()){
                        sqLiteDatabasedb.execSQL("UPDATE Setting SET MODE = 1");
                    }
                }else if(parent.getItemAtPosition(position).equals("무음모드")){
                    if(cursor.moveToFirst()){
                        sqLiteDatabasedb.execSQL("UPDATE Setting SET MODE = 2");
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    public void onClick(View view) {
        if (cursor.moveToFirst()) {
            String password = editText.getText().toString().trim();
            sqLiteDatabasedb.execSQL("UPDATE Setting SET PASSWORD =" + password);
        }
    }
}
